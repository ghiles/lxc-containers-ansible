Deploy Containers
=========

Create multiple unprivileged LXC container on multiple hosts via ansible.

Tasks
------------

Install LXC deamon and libvirt.

Enable user_namespace

Create specific containers for each specified host.

Set iptables to make containers reachable.



Role Variables
--------------

```YAML
auth_key: "/root/.ssh/id_rsa.pub"
containers:
  - container_name1:
    hote: host-A (must equal to inventory)
    cname: container_name
    lxc_dir: "/LXC-1"
    ip_pub: EXT_IP_TO_USE
  - container_name2:
    hote: host-A (must equal to inventory)
    cname: container_name
    lxc_dir: "/LXC-1"
    ip_pub: EXT_IP_TO_USE
  - container_name3:
    hote: host-B (must equal to inventory)
    cname: container_name
    lxc_dir: "/LXC-1"
    ip_pub: EXT_IP_TO_USE
lxc_template: centos
lxc_template_opts: "--release 7 --clean"
user: centops
svc: lxc

```

How to use
----------------
Add Container's hosts to group HCNT in hosts's file.
Configure All  vars in: group_vars/ HCNT.yml
and run the playbook: 

ansible-playbook -i hosts site.yml


License
-------

MIT
